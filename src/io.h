/*****************************************************************************/
/*   io.h                                               C. Haefner, 26.09.10 */
/*---------------------------------------------------------------------------*/
/*  Prototypen der terminal-Functions:                                       */
/*   - print_line                                                            */
/*   - print_status                                                          */
/*   - print_char                                                            */
/*   - print_error                                                           */
/*****************************************************************************/
#ifndef __IO_H
#define __IO_H

/****Funktionsprototypen****/

/*print_line*/
void print_line(char*);

/*print_status*/
void print_status(char*);

/*print_char*/
void print_char(char*);

/*print_error*/
void print_error(char*);

#endif
/*********************************** io.h ************************************/
