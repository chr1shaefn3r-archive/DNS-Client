#include <stdio.h>		// Standard I/O Funktionen wie printf()
#include <stdlib.h>		// Standard Funktionen wie exit() und malloc()
#include <string.h>		// String und Memory Funktionen wie strcmp() und memset()
#include <unistd.h>		// System Calls wie open(), read() und write()
#include <errno.h>		// Detailliertere Fehlermeldungen
#include <sys/socket.h>	// Socket Funktionen wie socket(), bind() und listen()
#include <arpa/inet.h>	// Funktionen wie inet_addr()
#include <netinet/in.h>	// IP Protokolle, sockaddr_in Struktur und Funktionen wie htons()
#include <netinet/ip.h>	// IP Header Struktur
#include <netinet/udp.h>// UDP Header Struktur
#include "io.h"			// Metasploitlike Output + Optionsparsing
#include <argp.h>		// Proper Commandlineoptions parsing

/*----------------------------------------------------------------------------*/
/* Globale Variablen definieren und definieren                                */
/*----------------------------------------------------------------------------*/
// Programm version
const char *argp_program_version = "dnslcient Beta";
// Report bugs to which eMail?
const char *argp_program_bug_address = "<bugs@die-haefners.de>";
// Program documentation
static char doc[] = "dnsclient -- a dnsclient which translates domains to ips";

// A description of the arguments we accept
static char args_doc[] = "<domainname>";

// The options we understand
static struct argp_option options[] = {
//    name          key arg    flags documentation
	{"verbose", 'v', 0, 0, "Produce verbose output"},
	{"destination", 'd', "IP", 0,
	 "Send the packet to a specific DNS-Server\n Default: 208.67.222.220 (openDNS)"},
	{"port", 'p', "PORT", 0,
	 "Use a specific sourceport\n Default: random()"},
	{0}			// Specific end (zero-terminated)
};

// Used by main to communicate with parse_opt
struct arguments {
	char *args[1];		/* arg1 = <domain> */
	int verbose, port;
	char *destination;
};

// Parse a single option
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	   know is a pointer to our arguments structure. */
	struct arguments *arguments = state->input;

	switch (key) {
	case 'v':
		arguments->verbose = 1;
		break;
	case 'd':
		arguments->destination = arg;
		break;
	case 'p':
		arguments->port = strtol(arg, NULL, 10);
		break;

	case ARGP_KEY_ARG:
		if (state->arg_num >= 1)
			/* Too many arguments. */
			argp_usage(state);
		arguments->args[state->arg_num] = arg;
		break;

	case ARGP_KEY_END:
		if (state->arg_num < 1)
			/* Not enough arguments. */
			argp_usage(state);
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

// Our argp parser
static struct argp argp = { options, parse_opt, args_doc, doc };

/*----------------------------------------------------------------------------*/
/* payload-Structure (needs to be extended for better useability with dns)    */
/*----------------------------------------------------------------------------*/
struct dnspayload {
	u_int16_t id;		// TransactionID
	u_int16_t flags;	// Flags
	u_int16_t queries;	// Number of Queries / Questions
	u_int16_t answers;	// Answer Resource Record
	u_int16_t auth_rr;	// Authority Resource Record
	u_int16_t add_rr;	// Additional Resource Record
	u_int8_t count;		// Anzahl der der Zeichen bis zum 1. Punkt
	char name[];		// Domainname
	//u_int16_t type;       // Querytype
	//u_int16_t class;      // Queryclass
};

// Main part
int main(int argc, char **argv)
{
	// fürs Options parsen
	struct arguments arguments;
	/*------------------------------------------------------------------------*/
	/* Standardwerte setzen                                                   */
	/*------------------------------------------------------------------------*/
	arguments.verbose = 0;
	arguments.destination = "208.67.222.220";
	arguments.port = random();
	arguments.port %= 65535;

	/*------------------------------------------------------------------------*/
	/* Parameterwerte parsen                                                  */
	/*------------------------------------------------------------------------*/
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	/*------------------------------------------------------------------------*/
	/* Variablen deklarieren (und definieren)                                 */
	/*------------------------------------------------------------------------*/
	// 4 Extrabytes, weil Type und Class Hardcoded hinten dran gehängt werden!
	int payloadsize =
	    sizeof(struct dnspayload) + strlen(arguments.args[0]) + 4;
	int i, j;		// für die For-Schleife(n)
	int sock;		// für die Sockets (Raw&Dgram)
	struct sockaddr_in addr;	// wird für den Absender im ersten Teil
	// und für die Empfangsmaske im zweiten Teil gebraucht
	unsigned int packetsize = sizeof(struct iphdr) + sizeof(struct udphdr) + payloadsize;	// Wie lang wird das Packet?
	unsigned char packet[packetsize];	// Platz für das Packet reservieren
	// Zeiger vom Typ iphdr auf das Packet erzeugen, dadurch kann die Struktur angesprochen werden
	// und die Daten landen in der richtigen Reihenfolge im Chararray "packet"
	struct iphdr *ip = (struct iphdr *)packet;
	struct udphdr *udp = (struct udphdr *)(packet + sizeof(struct iphdr));
	struct dnspayload *pay =
	    (struct dnspayload *)(packet + sizeof(struct iphdr) +
				  sizeof(struct udphdr));
	// fürs Packet empfangen
	char buffer[1024];	// Platz für das zu empfangende Packet machen
	ssize_t recsize;	// Länge des empfangenen Packets
	socklen_t fromlen;
	// für die Ausgaben
	int outputlength = 84;	// Länge Hardcoded, muss angepasst werden, falls Nachrichten abgeschnitten
	char outputmessage[outputlength];

	printf("  _____  _   _  _____    _____ _ _            _   \n");
	printf(" |  __ \\| \\ | |/ ____|  / ____| (_)          | |  \n");
	printf(" | |  | |  \\| | (___   | |    | |_  ___ _ __ | |_ \n");
	printf(" | |  | | . ` |\\___ \\  | |    | | |/ _ \\ '_ \\| __|\n");
	printf(" | |__| | |\\  |____) | | |____| | |  __/ | | | |_ \n");
	printf(" |_____/|_| \\_|_____/   \\_____|_|_|\\___|_| |_|\\__|\n");
	printf("\n");
	printf(" |_        /\\  | o  _  ._  \n");
	printf(" |_) \\/   /--\\ | | (/_ | | \n");
	printf("     /                     \n");

	if (arguments.verbose) {
		snprintf(outputmessage, outputlength,
			 "Übergebene Domain %s, hat Laenge %d",
			 arguments.args[0], strlen(arguments.args[0]));
		print_status(outputmessage);
		snprintf(outputmessage, outputlength, "Payloadlaenge: %d",
			 payloadsize);
		print_status(outputmessage);
	}

	/*------------------------------------------------------------------------*/
	/* Are we root? Otherwise exit!                                           */
	/*------------------------------------------------------------------------*/
	if (getuid() != 0) {
		print_error("You must have UID 0 (root)");
		exit(1);
	} else {
		if (arguments.verbose) {
			print_status("We are root, continue ...");
		}
	}

	/*------------------------------------------------------------------------*/
	/* Packetbuffer initialisieren                                            */
	/*------------------------------------------------------------------------*/
	memset(packet, '\0', packetsize);

	/*------------------------------------------------------------------------*/
	/* RAW-Socket erstellen                                                   */
	/*------------------------------------------------------------------------*/
	if ((sock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		print_error("Socket failed");
		exit(1);
	} else {
		if (arguments.verbose) {
			print_status("Socket created successfully");
		}
	}

	/*------------------------------------------------------------------------*/
	/* Socketoption IP_HDRINCL setzen damit der Kernel die Finger vom         */
	/* IP-Header lässt und uns nicht reinpfuscht                              */
	/*------------------------------------------------------------------------*/
	i = 1;
	if (setsockopt(sock, IPPROTO_IP, IP_HDRINCL, &i, sizeof(i)) == -1) {
		print_error("Failed to set socketoptions");
		exit(1);
	} else {
		if (arguments.verbose) {
			print_status("Sockeoptions set successfully");
		}
	}

	/*------------------------------------------------------------------------*/
	/* IP-Header zusammenbauen (wird automatisch in "packet" gespeichert)     */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Started to assemble IP-Header");
	};
	ip->version = 4;	// IP Version
	ip->ihl = 5;		// Internet Header Length
	ip->id = htonl(random());	// IP ID
//      ip->saddr = inet_addr("192.168.0.1"); // Source IP kann gesetzt werden ansonsten wird die eigene genutzt
	ip->daddr = inet_addr(arguments.destination);	// Destination IP
	ip->ttl = 123;		// Time to live
	ip->protocol = IPPROTO_UDP;	// Transport Protokoll UDP
	ip->tot_len = packetsize;	// Groesse des IP Pakets
	ip->check = 0;		// IP Checksum (Wenn die Checksumme 0 ist, wird sie vom Kernel berechnet)
	if (arguments.verbose) {
		print_status("Finished IP-Header assembling");
	};

	/*------------------------------------------------------------------------*/
	/* UDP-Header zusammenbauen (wird automatisch in "packet" gespeichert)    */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Started to assemble UDP-Header");
	};
	udp->source = htons(arguments.port);	// Source port
	udp->dest = htons(53);	// Destination port
	udp->len = htons(sizeof(struct udphdr) + payloadsize);	// UDP length
	udp->check = 0;		// IP Checksum (Wenn die Checksumme 0 ist, wird sie vom Kernel berechnet)
	if (arguments.verbose) {
		print_status("Finished UDP-Header assembling");
	};

	/*------------------------------------------------------------------------*/
	/* UDP-Payload zusammenbauen (wird automatisch in "packet" gespeichert)   */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Started to assemble UDP-Payload");
	};
	pay->id = random();
	pay->flags = 1;		// => Ergibt 01 00 im Packet
	pay->queries = 256;	// => Ergibt 00 01 im Packet
	pay->answers = 0;
	pay->auth_rr = 0;
	pay->add_rr = 0;
	/*------------------------------------------------------------------------*/
	/* Punkte durch die Anzahl der folgenden Zeichen ersetzen                 */
	/*------------------------------------------------------------------------*/
	// String rückwärts durcharbeiten
	for (j = 0, i = strlen(arguments.args[0]) - 1; i >= 0; i--, j++) {
		if (arguments.args[0][i] == '\x2e') {
			arguments.args[0][i] = j;
			if (arguments.verbose) {
				snprintf(outputmessage, outputlength,
					 "Punkt durch \"%d\" ersetzt!", j);
				print_status(outputmessage);
			}
			j = -1;
		} else {
			if (arguments.verbose) {
				print_char(".");
			}
		}
	}
	printf("\n");
	pay->count = j;
	memcpy(pay->name, arguments.args[0], strlen(arguments.args[0]));
	// Type und Class noch hinten dran haengen (jeweils 2Byte)
	memcpy(pay->name + strlen(arguments.args[0]) + 1, "\x00\x01\x00\x01",
	       4);
	if (arguments.verbose) {
		print_status("Finished UDP-Payload assembling");
	};

	/*------------------------------------------------------------------------*/
	/* Weitere Informatioen in "addr" speichern / werden an sendto übergeben  */
	/*------------------------------------------------------------------------*/
	addr.sin_family = AF_INET;
	addr.sin_port = udp->source;
//      addr.sin_addr.s_addr = ip->saddr; Kann gesetzt werden, ansonsten wird
//                                                                        eigene genutzt

	if (arguments.verbose) {
		print_status("Packetcontent:");
		for (i = 0; i < packetsize; i++) {
			snprintf(outputmessage, outputlength, "%.2x",
				 packet[i]);
			print_char(outputmessage);
		}
		printf("\n");
	}

	/*------------------------------------------------------------------------*/
	/* Packet auf die Reise schicken                                          */
	/*------------------------------------------------------------------------*/
	if ((sendto
	     (sock, packet, packetsize, 0, (struct sockaddr *)&addr,
	      sizeof(struct sockaddr_in))) == -1) {
		print_error("Failed to send packet");
		snprintf(outputmessage, outputlength,
			 "Cannot send message:  %s\n", strerror(errno));
		print_error(outputmessage);
		exit(1);
	} else {
		if (arguments.verbose) {
			print_status("Packet send successfully");
		}
	}

	/*------------------------------------------------------------------------*/
	/* Rawsocket schließen                                                    */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Closing rawsocket");
	};
	close(sock);
	if (arguments.verbose) {
		snprintf(outputmessage, outputlength,
			 "Socket closed, starting listening on Port %d",
			 arguments.port);
		print_status(outputmessage);
	};

	/*------------------------------------------------------------------------*/
	/* Packet empfangen um DNS-Antwort zu erhalten                            */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Speicher initialisieren");
	};
	memset(buffer, '\0', 1024);
	memset(&addr, '\0', sizeof addr);
	sock = 0;

	/*------------------------------------------------------------------------*/
	/* Empfangsmaske definieren / IPv4 & Port & IP-Adresse                    */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Building receivemask");
	};
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(arguments.port);

	/*------------------------------------------------------------------------*/
	/* Socket erstellen                                                       */
	/*------------------------------------------------------------------------*/
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		print_error("Socket failed");
		exit(1);
	} else {
		if (arguments.verbose) {
			print_status("Socket created successfully");
		}
	}

	/*------------------------------------------------------------------------*/
	/* Socket auf PORT binden                                                 */
	/*------------------------------------------------------------------------*/
	if (arguments.verbose) {
		print_status("Bindng socket");
	};
	if (bind(sock, (struct sockaddr *)&addr, sizeof addr) == -1) {
		print_error("Failed to bind socket");
		snprintf(outputmessage, outputlength, "Cannot bind:  %s\n",
			 strerror(errno));
		print_error(outputmessage);
		exit(1);
	} else {
		if (arguments.verbose) {
			print_status("Socket binded succesfully");
		}
	}

	/*------------------------------------------------------------------------*/
	/* Auf Antwortpacket warten                                               */
	/*------------------------------------------------------------------------*/
	recsize =
	    recvfrom(sock, (void *)buffer, 1024, 0, (struct sockaddr *)&addr,
		     &fromlen);

	/*------------------------------------------------------------------------*/
	/* Empfangenes Packet überprüfen                                          */
	/*------------------------------------------------------------------------*/
	if (recsize < 0) {
		print_error("Failed ro receive packet:");
		snprintf(outputmessage, outputlength, "%s\n", strerror(errno));
		print_error(outputmessage);
	} else {
		if (arguments.verbose) {
			print_status("Packet received sucessfully");
		}
	}

	if (arguments.verbose) {
		print_status("Packetcontent:");
		for (i = 0; i < recsize; i++) {
			snprintf(outputmessage, outputlength, "%.2x",
				 buffer[i]);
			print_char(outputmessage);
		}
		printf("\n");
	}

	/*------------------------------------------------------------------------*/
	/* TODO: IP aus Packet filtern                                            */
	/*------------------------------------------------------------------------*/
	printf("Domain has address %d.%d.%d.%d\n", buffer[recsize - 4],
	       buffer[recsize - 3], buffer[recsize - 2], buffer[recsize - 1]);

	// Everything work finde, return 0 :)
	return 0;
}
