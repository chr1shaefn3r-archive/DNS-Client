#!/bin/sh

#
# Helps generate autoconf/automake stuff, when code is checked out from SCM.
#

(autoconf --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have autoconf installed to generate util-linux-ng build system."
	echo
	echo "Download the appropriate package for your distribution,"
	echo "or see http://www.gnu.org/software/autoconf"
	exit 1;
}
(automake --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have automake installed to generate util-linux-ng build system."
	echo 
	echo "Download the appropriate package for your distribution,"
	echo "or see http://www.gnu.org/software/automake"
	exit 1;
}
(autoheader --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have autoheader installed to generate util-linux-ng build system."
	echo 
	echo "Download the appropriate package for your distribution,"
	echo "or see http://www.gnu.org/software/autoheader"
	exit 1;
}

test -f src/dnsclient.c || {
	echo "You must run this script in the top-level util-linux-ng directory"
	exit 1
}

echo
echo "Generate build-system by:"
echo "   aclocal:    $(aclocal --version | head -1)"
echo "   autoconf:   $(autoconf --version | head -1)"
echo "   autoheader: $(autoheader --version | head -1)"
echo "   automake:   $(automake --version | head -1)"

set -e

aclocal
autoconf
autoheader

automake --add-missing

echo
echo "Now type '/.configure' and 'make' to compile."
echo
